import datetime
from floodsystem.flood import stations_highest_rel_level
from floodsystem.plot import plot_water_levels, plot_water_level_with_fit
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.datafetcher import fetch_measure_levels
import matplotlib.pyplot as plt

def run():
    """Task 2F requirements"""
    DT = 2
    N = 5
    p = 4

    stations = build_station_list()
    update_water_levels(stations)
    # Get the station objects out of the list of names
    risky_stations = stations_highest_rel_level(stations, N)

    for station in risky_stations:
        dates, levels = fetch_measure_levels(
            station.measure_id, dt=datetime.timedelta(days=DT))
        if len(dates) == 0 or len(levels) == 0:
            continue  # Deal with empty lists appearing
        plot_water_level_with_fit(station, dates, levels, p)
        plot_water_levels(station, dates, levels)
        plt.show()

if __name__ == "__main__":
    print("*** Task 2F: CUED Part IA Flood Warning System *** \n")

    # Run Task2F
    run()