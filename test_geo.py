from floodsystem.stationdata import build_station_list

from floodsystem.geo import *
from floodsystem.station import MonitoringStation

def test_stations_by_distance():
    # Getting stations to test with
    stations = build_station_list()
    station_distances = stations_by_distance(stations, (52.2053, 0.1218))

    # Is `station_distances` a list?
    assert isinstance(station_distances, list)
    # Are both lists the same size?
    assert len(station_distances) == len(stations)

    for val in station_distances:
        assert isinstance(val, tuple)
        assert len(val) == 2
        # Is the first value a station?
        assert isinstance(val[0], MonitoringStation)
        # Is the second value a float?
        assert isinstance(val[1], float)
    # Is the distance larger than the one before?
    for i in range(len(station_distances) - 1):
        assert station_distances[i][1] <= station_distances[i+1][1]

def test_stations_within_radius():
    stations = build_station_list() # Put this line at the top of each of your test functions
    stations_within_radius_output = stations_within_radius(stations, (52.2053, 0.1218), 100) 
    for val in stations_within_radius_output:
        #is the value a station
        assert isinstance(val, MonitoringStation)

def test_rivers_with_stations():
    stations = build_station_list()
    rivers_with_station_output = rivers_with_station(stations)
    assert isinstance(rivers_with_station_output, list)
    for val in rivers_with_station_output:
        #check value is a string (rivers)
        assert isinstance(val, str)

def test_stations_by_river():
    stations = build_station_list()
    stations_by_river_output = stations_by_river(stations)
    assert type(stations_by_river_output) == dict

def test_rivers_by_station_number():
    stations = build_station_list()
    rivers_by_station_number_output = rivers_by_station_number(stations, 9)
    assert type(rivers_by_station_number_output) == list
    for val in rivers_by_station_number_output:
        #checks that first tuple entry is a string
        assert isinstance(val[0], str)
        #checks that the numbers are integers
        assert isinstance(val[1], int)
    #checks that the values are decreasing
    for i in range(len(rivers_by_station_number_output) - 1):
        assert rivers_by_station_number_output[i][1] >= rivers_by_station_number_output[i+1][1]

if __name__ == "__main__": # This is for debugging, so that when this file is run manually it runs the test functions.
    test_stations_by_distance()
    test_rivers_by_station_number()
    test_rivers_with_stations()
    test_stations_by_river()
    test_stations_within_radius()