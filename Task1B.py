from floodsystem.stationdata import build_station_list
stations = build_station_list()
from floodsystem.geo import stations_by_distance

station_distances = stations_by_distance(stations, (52.2053, 0.1218))
first_10 = station_distances[:10]
last_10 = station_distances[-10:]
first_last_20 = first_10 + last_10

# "For each tuple in `first_last_20`, set the first value to `station` and the second to `distance`."
for station, distance in first_last_20:
    print(station.name, station.town, distance)