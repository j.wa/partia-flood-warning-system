# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module contains a collection of functions related to
geographical data.

"""
from math import radians, cos, sin, asin, sqrt
from .utils import sorted_by_key
import operator

def haversine(p1, p2):
    """
    Calculate the great circle distance between two points 
    on the earth (specified in decimal degrees)
    """
    # convert decimal degrees to radians 
    lon1, lat1 = p1
    lon2, lat2 = p2
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])

    # haversine formula 
    dlon = lon2 - lon1 
    dlat = lat2 - lat1 
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
    c = 2 * asin(sqrt(a)) 
    r = 6371 # Radius of earth in kilometers. Use 3956 for miles
    return c * r

def stations_by_distance(stations, p):
    """Return a list of tuples, with stations and their distance from coordinate `p`, sorted by distance."""
    station_distances = [(station, haversine(p, station.coord)) for station in stations]
    station_distances = sorted_by_key(station_distances, 1)
    return station_distances


def stations_within_radius(stations, centre, r):
    """Returns all stations from `stations` within radius `r` of a geographic coordinate `centre`."""

    def is_within_radius(station):
        """Return True if a station is within radius r of the centre."""
        return haversine(centre, station.coord) <= r

    # Returns a list of all stations, which when passed into is_within_radius return True.
    return list(filter(is_within_radius, stations))

def rivers_with_station(stations):
    """Returns a container list with the names of the rivers with a monitoring station in `stations`."""
    # A set is like a list but you can't have multiple copies of the same item and the items aren't ordered.
    rivers = set()
    for station in stations:
        rivers.add(station.river)
    return list(rivers)

def stations_by_river(stations):
    """Returns a Dict that maps river names to a list of stations on a given river."""
    river_stations = {}

    # For each river that has at least one station, add an entry for it to the Dict.
    for river in rivers_with_station(stations):
        river_stations[river] = []

    # For each station, add it to the Dict entry of the river it is monitoring.
    for station in stations:
        river_stations[station.river].append(station)
    
    return river_stations

def rivers_by_station_number(stations, N):
    """Returns tuples in descending order of the number of stations belonging to a river"""
    #outputs the value of the Nth key in a dictionary
    def N_val(N):
        return list(sorted(d.values(), reverse = True))[N-1]

    #create a dictionary of rivers and tally of number of monitoring stations
    d = {}
    for station in stations:
        river = station.river
        if river in d:
            d[river] += 1
        else:
            d[river] = 1

    #turn into sorted list based on descending tally value
    complete_list = sorted([(a,b) for a,b in d.items()], reverse=True, key=lambda x: x[1])
    trim_list = complete_list[:N+1]

    #check for values outside of N with same tally as Nth river
    i = N+1
    while N_val(i) == N_val(i+1):
        trim_list.append(complete_list[i])
        i += 1
    return trim_list