# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module provides a model for a monitoring station, and tools
for manipulating/modifying station data

"""


class MonitoringStation:
    """This class represents a river level monitoring station"""

    def __init__(self, station_id, measure_id, label, coord, typical_range,
                 river, town):

        self.station_id = station_id
        self.measure_id = measure_id

        # Handle case of erroneous data where data system returns
        # '[label, label]' rather than 'label'
        if isinstance(label, list):
            self.name = label[0]
        else:
            self.name = label

        self.coord = coord
        self.typical_range = typical_range
        self.river = river
        self.town = town

        self.latest_level = None

    def __repr__(self):
        d = "Station name:     {}\n".format(self.name)
        d += "   id:            {}\n".format(self.station_id)
        d += "   measure id:    {}\n".format(self.measure_id)
        d += "   coordinate:    {}\n".format(self.coord)
        d += "   town:          {}\n".format(self.town)
        d += "   river:         {}\n".format(self.river)
        d += "   typical range: {}".format(self.typical_range)
        return d

    def relative_water_level(self):
        """Returns the latest water level as a fraction of the typical range, and returns None if data 
        not available"""

        if self.typical_range_consistent():

            # the following raises an exception if latestlevel is None kand if any other errors occur
            try:
                relative_level = (self.latest_level - self.typical_range[0]) / (self.typical_range[1] - self.typical_range[0])

            except:
                relative_level = None

        else:
            relative_level = None

        return relative_level

    def latest_level_consistent(self):
        """Function to deal with weird cases such as latest level being a list, 
        also just checks that update water levels went correctly for the latest level"""
        if self.latest_level is None:
            return False

        if not isinstance(self.latest_level, float):
            return False  # For the random stations that have a list as latest

        return True
        
    def typical_range_consistent(self):                             # function is part of this class system 
        try:                                                        # try function attempts the function below first 
            if len(self.typical_range) != 2:
                return False
            return self.typical_range[1] > self.typical_range[0]    # typical range is in tuple - sift into tuple and find difference
        except:                                                     # there is an issue if range from max to min is negative 
            return False                                            # if the try didnt work lable the value false 


def inconsistent_typical_range_stations(stations):          # now we need to find the stations which have errors 
    inconsistent = []                                       # create an empty list for the issue stations
    for station in stations:                                # filter through all the different stations
        if not station.typical_range_consistent():          # if there is a false range value for a station, pull it out 
            inconsistent.append(station)                    # add these trouble stations to a list 
    return inconsistent                                     # this function puts all the info on that station into a list