def stations_level_over_threshold(stations, tol):
    """Returns a list of the tuples, each containing the name of a station at which the relative
    water level is above tol and the relative water level at that station"""

    flooded_stations = []

    for station in stations:
        if not station.typical_range_consistent() or not station.latest_level_consistent():
            continue
        relative_level = station.relative_water_level()
        # the following raises an exception if relative_level is None
        if relative_level is None:
            continue
        if relative_level > tol:
            flooded_stations.append((station, relative_level))

    # sort list by second value in descending order
    flooded_stations.sort(key=lambda x: x[1], reverse=True)
    return flooded_stations

def stations_highest_rel_level(stations, N):
    """Returns a list of the N stations at which the water level,
    relative to the typical range, is highest"""

    stations_relative_level = []

    for station in stations:
        relative_level = station.relative_water_level()

        # only append if relative level is valid data
        if relative_level != None:
            stations_relative_level.append((station, relative_level))

    stations_relative_level.sort(key=lambda x: x[1], reverse=True) #sorting the list
    flooded_stations = stations_relative_level[:N] #cutting the list down to the first N objects
    for i, s in enumerate(flooded_stations): # Making the list just include the stations
        flooded_stations[i] = s[0]
        

    return flooded_stations