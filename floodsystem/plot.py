import matplotlib.pyplot as plt
import dateutil.parser
import datetime
import numpy as np
from .stationdata import build_station_list
from .analysis import polyfit
import floodsystem.geo
import matplotlib

def plot_water_levels(station, dates, levels):

    plt.plot(dates, levels, color = 'red',label="$water levels$")

    plt.xlabel('date')
    plt.ylabel('water level (m)')
    plt.xticks(rotation=45)
    plt.title("Station: " + station.name)

    plt.tight_layout()

    plt.show()


def plot_water_level_with_fit(station, dates, levels, p):
    """Plots the historical water levels of station given by dates and levels.
    It then plots those levels along with a polynomial best fit of degree
    p which can then be used to estimate future water levels"""
    plot_water_levels(station, dates, levels)
    best_fit, offset = polyfit(dates, levels, p)

    x = matplotlib.dates.date2num(dates)
    y = best_fit(x - offset)
    plt.plot(dates, y, label="$Best Fit$")
    plt.legend(loc=2)