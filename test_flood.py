
from floodsystem.station import MonitoringStation
from floodsystem.flood import stations_level_over_threshold, stations_highest_rel_level

def test_stations_level_over_threshold():
    stations = get_test_stations()

    out1 = stations_level_over_threshold(stations, 0.4)
    assert len(out1) == 3
    assert out1[0][0].name == "f" and out1[1][0].name == "e" and out1[2][0].name == "c"

    out2 = stations_level_over_threshold(stations, 1.2)
    assert len(out2) == 0

def test_stations_highest_rel_level():
    stations = get_test_stations()

    out1 = stations_highest_rel_level(stations, 3)
    assert len(out1) == 3
    assert out1[0].name == "f" and out1[1].name == "e" and out1[2].name == "c"
    out2 = stations_highest_rel_level(stations, 0)
    assert len(out2) == 0

def get_test_stations():
    stations = []
    i = 1.0
    for ch in "abcdefg":
        s = MonitoringStation(ch, ch, ch, (0.0, 0.0), (0.0, 5.0), ch, ch)
        s.latest_level = i
        stations.append(s)

        i += 1.0
    
    stations[6].typical_range = (3, 2)
    stations[3].latest_level = -6
    stations[1].typical_range = None
    return stations

if __name__ == "__main__":
    test_stations_level_over_threshold()
    test_stations_highest_rel_level()