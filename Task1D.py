from floodsystem.stationdata import build_station_list
from floodsystem import geo

stations = build_station_list()



rivers = geo.rivers_with_station(stations)
print("Rivers:", len(rivers)) # Print how many rivers are being monitored
print(sorted(rivers)[:10]) # The first 10 rivers by alphabetical order



stations_by_river = geo.stations_by_river(stations)

test_rivers = ("River Aire", "River Cam", "River Thames")
for river in test_rivers:
    station_names = [station.name for station in stations_by_river[river]]
    print(sorted(station_names))