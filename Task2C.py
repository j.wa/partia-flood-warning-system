from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.flood import stations_highest_rel_level

def run():
    """Task 2C requirements"""

    # build list of stations
    stations = build_station_list()
    update_water_levels(stations)
    N=10

    # create a list of stations with levels over the tolerance
    flooded_stations=stations_highest_rel_level(stations,N)
    
    # print the list
    for station in flooded_stations:
        print(station.name, station.relative_water_level())

if __name__ == "__main__":
    print("*** Task 2C: CUED Part IA Flood Warning System *** \n")

    # Run Task2C
    run()