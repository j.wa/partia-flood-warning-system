import matplotlib.pyplot as plt
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.plot import plot_water_levels
from floodsystem.flood import stations_highest_rel_level
import datetime


def run():
    """task 2E"""

    N = 5
    dt = 10
    stations = build_station_list()
    update_water_levels(stations)

        # build list of tuples of looded stations
    flooded_stations = stations_highest_rel_level(stations, N)

    # plot data for each station
    for station in flooded_stations:
        dates, levels = fetch_measure_levels(
            station.measure_id, dt=datetime.timedelta(days=dt))
        if len(dates) == 0 or len(levels) == 0:
            continue  # Test for the stations with incorrect datafetcher responses
        plot_water_levels(station, dates, levels)
        plt.show()

if __name__ == "__main__":
    print("*** Task 2E: CUED Part IA Flood Warning System *** \n")
    run()