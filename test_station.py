# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""Unit test for the station module"""

from floodsystem.station import MonitoringStation, inconsistent_typical_range_stations
from floodsystem.stationdata import build_station_list, update_water_levels

def test_create_monitoring_station():

    # Create a station
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    trange = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    s = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    assert s.station_id == s_id
    assert s.measure_id == m_id
    assert s.name == label
    assert s.coord == coord
    assert s.typical_range == trange
    assert s.river == river
    assert s.town == town

def test_typical_range_consistent():
    """Tests the consistency tester function for the station class"""

    s1 = MonitoringStation(None, None, None, None, None, None, None)
    assert s1.typical_range_consistent() == False
    
    # Below relies on web functions to work, so is commented out
    """
    stations = build_station_list()
    update_water_levels(stations)
    for s in stations:
        if s.typical_range_consistent():
            assert (s.typical_range[1] >= s.typical_range[0])
    """

    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "test station"
    coord = (-2.0, 4.0)
    trange = (-2.3, 3.4445)
    river = "test river"
    town = "test town"
    s2 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    
    # Testing multiple ranges
    test_ranges = ((3,2), (2.10, -2.3), (2,3,4), 2, (-2.3, 3.4445))
    test_consistent = (False, False, False, False, True)

    for trange, consis in zip(test_ranges, test_consistent):
        s2.typical_range = trange
        assert s2.typical_range_consistent() == consis

def test_inconsistent_typical_range_stations():
    """Ensures that all the stations returned by inconsistent_typical_range_stations
    are actually inconsistent (assumes that inconsistent testing works correctly)"""
    
    stations = build_station_list()
    inconsistent_stations = inconsistent_typical_range_stations(stations)
    for s in inconsistent_stations:
        assert not s.typical_range_consistent()

def test_relative_water_level():
    s_id = "test-s-id"
    m_id = "test-m-id"
    trange = (0, 0)
    s = MonitoringStation(s_id, m_id, "test station", (-2.0, 4.0), trange, "test river", "test town")
    
    data = (
        ((-2, 6), -2.0, 0.0),
        ((0, 100), 3.0, 0.03),
        ((-4, 6), 3.0, 0.7),
        ((-2, 18), 16.0, 0.9),
        ((-10, -5), -7.0, 0.6),
        ((-2, 6), 6, 1.0)
    )
    for trange, level, expected in data:
        s.typical_range = trange
        s.latest_level = level
        assert abs(s.relative_water_level()-expected) < 0.00001

if __name__ == "__main__":
    test_typical_range_consistent()
    test_inconsistent_typical_range_stations()
    test_relative_water_level()