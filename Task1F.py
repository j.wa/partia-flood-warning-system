from floodsystem.stationdata import build_station_list
stations = build_station_list()
from floodsystem.station import inconsistent_typical_range_stations

name_list = sorted([station.name for station in inconsistent_typical_range_stations(stations)])
print(name_list)